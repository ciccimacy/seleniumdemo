package com.cloudsherpas.seleniumdemo.skrowti.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SkrowtyTweetifyPage {
	public WebDriver driver;

	public SkrowtyTweetifyPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement findTextBox(){
		return driver.findElement(By.name("content"));
	}
	
	public WebElement findTweetButton(){
		return driver.findElement(By.cssSelector("input[type=\"submit\"]"));
	}
	
	public WebElement findLatestMsg(){
		return driver.findElement(By.xpath("//div[2]"));
	}
	
	
}
