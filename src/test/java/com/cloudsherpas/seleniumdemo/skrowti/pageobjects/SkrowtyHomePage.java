package com.cloudsherpas.seleniumdemo.skrowti.pageobjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SkrowtyHomePage {
public WebDriver driver;
private static String url = "http://www.skrowti.com/";
public SkrowtyHomePage(WebDriver driver) {
	this.driver = driver;
}

public void goToSite() {
	driver.get(url);
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
}

public WebElement findTwitterLink(){
	return driver.findElement(By.linkText("Twitter"));
}

public WebElement findCalculatorLink(){
	return driver.findElement(By.linkText("Calculator"));
}

public WebElement findChatLink(){
	return driver.findElement(By.linkText("Chat"));
}

}
