package com.cloudsherpas.seleniumdemo;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import com.cloudsherpas.seleniumdemo.gmail.pageobjects.GmailBox;
import com.cloudsherpas.seleniumdemo.gmail.pageobjects.GmailHomePage;
import com.cloudsherpas.seleniumdemo.test.base.TestBase;

public class SendGmailTest extends TestBase {
	private GmailBox gmailBox = new GmailBox(driver);

	@Test
	public void testSendEmail() throws InterruptedException {
		login("cs.testselenium@gmail.com", "seleniumide");
		composeEmail("cs.testselenium@gmail.com", "Subject", "Hello there");
		signout();
	}


	private void login(String username, String password) {
		GmailHomePage homePage = new GmailHomePage(driver);
		homePage.goToSite();

		homePage.findUserNameTextBox().clear();
		homePage.findUserNameTextBox().sendKeys("cs.testselenium@gmail.com");
		homePage.findUserNameNextButton().click();
		homePage.findPasswordTextBox().clear();
		homePage.findPasswordTextBox().sendKeys("seleniumide");
		homePage.findPasswordSignInButton().click();

	}

	private void composeEmail(String emailTo, String subject, String body)
			throws InterruptedException {

		for (int second = 0;; second++) {
			if (second >= 60)
				fail("timeout");
			try {
				if (isElementPresent(By
						.xpath("//div[@class='z0']/div")))
					break;
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		}

		gmailBox.findComposeButton().click();

		// Composing email
		for (int second = 0;; second++) {
			if (second >= 60)
				fail("timeout");
			try {
				if (isElementPresent(By
						.xpath("//textarea[@name='to']")))
					break;
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		}

		gmailBox.findToTextField().clear();
		gmailBox.findToTextField().sendKeys(emailTo);
		gmailBox.findSubjectTextField().clear();
		gmailBox.findSubjectTextField().sendKeys(subject);
		gmailBox.findBodyTextArea().clear();
		gmailBox.findBodyTextArea().sendKeys(body);

		// Send
		gmailBox.findSendButton().click();

	}

	private void signout() throws InterruptedException {
		for (int second = 0;; second++) {
			if (second >= 60)
				fail("timeout");
			try {
				if ("Your message has been sent. View message".equals(driver
						.findElement(By.cssSelector("div.vh")).getText()))
					break;
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		}

		gmailBox.findEmailAccountSettings("cs.testselenium@gmail.com").click();
		gmailBox.findSignOutLink().click();
	}
	
	public boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}
}