package com.cloudsherpas.seleniumdemo;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.Test;

import com.cloudsherpas.seleniumdemo.skrowti.pageobjects.SkrowtyHomePage;
import com.cloudsherpas.seleniumdemo.skrowti.pageobjects.SkrowtyTweetifyPage;
import com.cloudsherpas.seleniumdemo.test.base.TestBase;

public class SkrowtyTwitterTest extends TestBase {
	private SkrowtyHomePage homePage = new SkrowtyHomePage(driver);
	private SkrowtyTweetifyPage tweetifyPage = new SkrowtyTweetifyPage(driver);

	@Test
	public void testTweetify() {
		homePage.goToSite();
		homePage.findTwitterLink().click();

		tweetifyPage.findTextBox().clear();
		tweetifyPage.findTextBox().sendKeys("Hello beautiful");
		tweetifyPage.findTweetButton().click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		assertEquals("", tweetifyPage.findTextBox().getText());
		assertEquals("Hello beautiful", tweetifyPage
				.findLatestMsg().getText());
	}

}
