package com.cloudsherpas.seleniumdemo.gmail.pageobjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GmailHomePage {
	private WebDriver driver;
	private static String url = "http://www.gmail.com";
	
	public GmailHomePage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void goToSite() {
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	public WebElement findUserNameTextBox() {
		return driver.findElement(By.id("Email"));
	}
	
	public WebElement findPasswordTextBox() {
		return driver.findElement(By.id("Passwd"));
	}
	
	public WebElement findUserNameNextButton() {
		return driver.findElement(By.id("next"));
	}

	public WebElement findPasswordSignInButton() {
		return driver.findElement(By.id("signIn"));
	}
	
	
	public WebElement findSubjectInTheInbox(String text) {
		return driver.findElement(By.linkText(text));
	}
	
}
