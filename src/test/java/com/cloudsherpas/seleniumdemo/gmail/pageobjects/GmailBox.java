package com.cloudsherpas.seleniumdemo.gmail.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.*;


public class GmailBox {
	private WebDriver driver;
	
	public GmailBox(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement findComposeButton() {
		return driver.findElement(By.xpath("//div[@class='z0']/div"));
	}
	
	public WebElement findToTextField(){
		return driver.findElement(By.xpath("//textarea[@name='to']"));
	}
	
	public WebElement findSubjectTextField(){
		return driver.findElement(By.xpath("//input[@name='subjectbox']"));
	}
	
	public WebElement findBodyTextArea(){
		return driver.findElement(By.xpath("//div[@aria-label='Message Body']"));
	}
	
	public WebElement findSendButton(){
		return driver.findElement(By.xpath("//div[text()='Send']"));
	}
	
	public WebElement findEmailAccountSettings(String gmail){
		return driver.findElement(By.xpath(gmail));
	}
	
	public WebElement findSignOutLink(){
		return driver.findElement(By.xpath("gb_71"));
		
	}
	
	public WebElement findSubjectNameInTheInbox(String text) {
		return driver.findElement(By.linkText(text));
	}
	
	
}
