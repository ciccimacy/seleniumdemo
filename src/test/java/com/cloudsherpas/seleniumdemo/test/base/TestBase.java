package com.cloudsherpas.seleniumdemo.test.base;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class TestBase {
	
	  protected static WebDriver driver;
		private static String browser;
		
	 @BeforeClass
	    public static void setUpBeforeClass() throws Exception {
	    	
	    	browser = System.getProperty("browser").toUpperCase();
	    	if(browser.equals("CHROME")){
		    	//The path to the chrome driver
		        //Need to download the relevant driver from
		        String resourceName = "src/test/resources/chromedriver";

		        System.setProperty("webdriver.chrome.driver", resourceName);
		    	driver = new ChromeDriver();
		    }
		    else if (browser.equals("IE")){
		    	driver = new InternetExplorerDriver();
		    }
		    else{
		    	driver = new FirefoxDriver();	    	
		    }
	    }
	 
	    @AfterClass
	    public static void tearDownAfterClass() throws Exception {
	        driver.quit();
	        driver = null;
	    }
}
